package com.example.eheca.showlistapp

import android.content.Intent
import android.net.Uri
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.text.Html
import android.text.Spanned
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_show.*
import org.json.JSONArray
import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.InputStreamReader
import android.widget.ListView



class ShowActivity : AppCompatActivity() {
    private var id: Int = 0
    private var url: String = ""

    companion object {
        val id_key = "id"
        val image_key = "image"
        val name_key = "name"
        val url_key = "url"
        val description_key = "description"
        val network_key = "network"
        val days_key = "days"
        val time_key = "time"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show)

        //Set the toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)

        if (intent != null) {
            id = intent.getIntExtra(id_key, 0)
            url = intent.getStringExtra(url_key)
            //Load all data values in activity_show
            supportActionBar?.title = intent.getStringExtra(name_key)
            title_TextView.text = intent.getStringExtra(name_key)
            var description = intent.getStringExtra(description_key)
            val spanned: Spanned = Html.fromHtml(description)
            description_TextView.text = spanned
            network_TextView.text = intent.getStringExtra(network_key)
            days_TextView.text = intent.getStringExtra(days_key)
            time_TextView.text = intent.getStringExtra(time_key)

            //Load image show in show_ImageView
            val imageUrl = intent.getStringExtra(image_key)
            if (!imageUrl.isEmpty())
                Picasso.get().load(imageUrl).into(show_ImageView)
            else
                Picasso.get().load(R.drawable.app_icon).into(show_ImageView)
        }
        val episodesAdapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1)
        episodes_ListView.adapter = episodesAdapter
    }

    override fun onStart() {
        super.onStart()
        go_button.setOnClickListener {
            //If don't have official link show a snackbar
            if (url.isEmpty() || url == "null") {
                Snackbar.make(it, "Este show no tiene sitio :c", Snackbar.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            //Open the lin in the internet navigator
            val uri = Uri.parse(url)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }
        var apiUrl = "http://api.tvmaze.com/shows/serie_id/episodes"
        apiUrl = apiUrl.replace("serie_id", id.toString())
        //Get the APIData
        println(apiUrl)
        GetApiData(this, apiUrl).execute()
    }

    fun setListViewHeightBasedOnChildren(listView: ListView) {
        val listAdapter = listView.adapter
                ?: // pre-condition
                return

        var totalHeight = 0
        for (i in 0 until listAdapter.count) {
            val listItem = listAdapter.getView(i, null, listView)
            listItem.measure(0, 0)
            totalHeight += listItem.getMeasuredHeight()
        }

        val params = listView.layoutParams
        params.height = totalHeight + listView.dividerHeight * (listAdapter.count - 1)
        listView.layoutParams = params
        listView.requestLayout()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return false
    }

    //Async task to load the Api data
    open class GetApiData(var showActivity: ShowActivity, var apiUrl: String) : AsyncTask<Unit, Unit, String>() {
        val listOfEpisodes = mutableListOf<Episode>()
        val seasons = mutableListOf<Int>()

        override fun doInBackground(vararg p0: Unit?): String {
            //Start the ApiClient for search the show episodes
            val apiClient = APIClient()
            val stream = BufferedInputStream(apiClient.getJson(apiUrl!!))
            return readStream(stream)
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            for (episode in getApiData(result)){
                listOfEpisodes.add(episode)
                //Filter only one season item for the list of seasons
                if (!seasons.contains(episode.season))
                    seasons.add(episode.season)
            }
            //Load the seasons List
            val seasonsAdapter: ArrayAdapter<Int> = ArrayAdapter(showActivity, android.R.layout.simple_list_item_1)
            seasonsAdapter.addAll(seasons)
            seasonsAdapter.notifyDataSetChanged()
            showActivity.seasons_Spinner.adapter = seasonsAdapter

            //Add the listener for the spinner to select the episodes per season
            showActivity.seasons_Spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {}

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    //Clear the showActivity.episodes_ListView
                    (showActivity.episodes_ListView.adapter as ArrayAdapter<String>).clear()
                    //Get the number of the season
                    val season: Int = (parent?.adapter as ArrayAdapter<Int>).getItem(position)
                    //Filter the episodes by season
                    for (episode in listOfEpisodes)
                    //Load the tv shows in MainActivity.shows_RecyclerView
                        if (episode.season == season)
                            (showActivity.episodes_ListView.adapter as ArrayAdapter<String>).add("${episode.number} - ${episode.name}")
                    //Notify the changes in the adapter
                    (showActivity.episodes_ListView.adapter as ArrayAdapter<String>).notifyDataSetChanged()
                    showActivity.setListViewHeightBasedOnChildren(showActivity.episodes_ListView)
                }
            }
        }

        private fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        private fun getApiData(result: String?): MutableList<Episode> {
            val episodeList = mutableListOf<Episode>()
            var jsonArray = JSONArray(result)

            //Get the data of the json
            for (pos in 0..(jsonArray.length() - 1)) {
                //Get the Json Object
                val jsonObject = jsonArray.getJSONObject(pos)
                //Create a new Episode
                val episode = Episode()
                //Get the api data
                episode.name = jsonObject.getString("name")
                episode.number = jsonObject.getInt("number")
                episode.season = jsonObject.getInt("season")

                episodeList.add(episode)
            }
            //return the list of the episodes
            return episodeList
        }
    }
}
