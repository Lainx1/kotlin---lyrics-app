package com.example.eheca.showlistapp

class Show {
    var id: Int = 0
    var url: String = ""
    var imageUrl: String = ""
    var name: String = ""
    var description: String = ""
    var networkName: String = ""
    var time: String = ""
    var days = mutableListOf<String>()
}