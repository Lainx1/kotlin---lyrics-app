package com.example.eheca.showlistapp

import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AppCompatDelegate
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.InputStreamReader

class MainActivity : AppCompatActivity() , TextWatcher{
    private var layoutManager:  RecyclerView.LayoutManager? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        //Set the night mode
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //Toolbar
        setSupportActionBar(toolbar)
        //Set the adapter and layout manager into shows_RecyclerView
        //Set the grid layout manager
        layoutManager = GridLayoutManager(this, 3)//Three columns
        shows_RecyclerView.layoutManager = layoutManager
        //Set the adapter
        var adapter = ShowAdapter(mutableListOf())
        shows_RecyclerView.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        showName_EditText.addTextChangedListener(this)
        //Clear the adapter
    }
    //Load the show info if the user write something in showName_EditText
    override fun afterTextChanged(editable: Editable?) {
        val showName = editable.toString()
        //The APIurl
        var apiUrl = "http://api.tvmaze.com/search/shows?q=text"
        apiUrl = apiUrl!!.replace("text", showName)
        //Get the APIData
        GetApiData(shows_RecyclerView.adapter as ShowAdapter, apiUrl).execute()
    }
    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

    override fun onTextChanged(text: CharSequence?, after: Int, before: Int, count: Int) {
    }
    //Async task to load the Api data
    open class GetApiData(var showAdapter: ShowAdapter,var apiUrl: String): AsyncTask<Unit, Unit, String>(){

        override fun doInBackground(vararg p0: Unit?): String{
            //Start the ApiClient to search the shows
            val apiClient = APIClient()
            val stream = BufferedInputStream(apiClient.getJson(apiUrl!!))
            return  readStream(stream)
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            //Load the tv shows in MainActivity.shows_RecyclerView
            showAdapter.addShows(getApiData(result))
        }
        private fun readStream(inputStream: BufferedInputStream): String{
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }
        private fun getApiData(result: String?): MutableList<Show>{
            val listOfShows = mutableListOf<Show>()
            var jsonArray = JSONArray(result)

            //Get the data of the json
            for (pos in 0..(jsonArray.length() -1)){
                //Get the Json Object
                val jsonObject = jsonArray.getJSONObject(pos)
                //Get the show object
                val showJsonObject = jsonObject.getJSONObject("show")

                //Create a new show
                val show = Show()

                show.id = showJsonObject.getInt("id")
                show.name = showJsonObject.getString("name")
                show.description = showJsonObject.getString("summary")
                show.url = showJsonObject.getString("officialSite")

                //Get the schedule
                val scheduleJsonObject = showJsonObject.getJSONObject("schedule")
                //Get the days and time
                show.time = scheduleJsonObject.getString("time")
                //Get the days
                val daysJsonArray =  scheduleJsonObject.getJSONArray("days")
                for(day in 0..(daysJsonArray.length() - 1)){
                    show.days.add(daysJsonArray.getString(day))
                }

                //Get the network info

                //Get the name of the network
                if  (showJsonObject.get("network").toString() != "null"){
                    val networkJsonObject  = showJsonObject.getJSONObject("network")
                    show.networkName = networkJsonObject.getString("name")
                }

                //Get the image
                if(showJsonObject.get("image").toString() != "null"){
                    val imageJsonObject = showJsonObject.getJSONObject("image")
                    show.imageUrl = imageJsonObject.getString("medium")
                }

                //add the show to the list
                listOfShows.add(show)
            }
            // return the list of shows
            return listOfShows
        }
    }
}
